
# Ambiente Laravel

## Utilitários

A pasta `./bin` possui diversos scripts utilitários.

Para configurar o projeto, execute:

```
./bin/config
```

Após a configuração, para iniciar a aplicação, execute:

```
./bin/up
```

